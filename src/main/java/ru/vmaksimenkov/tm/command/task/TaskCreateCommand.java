package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getTaskService().add(userId, name, TerminalUtil.nextLine());
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

}
