package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
